﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using Library;
using System.Xml.Linq;
using System.IO;

namespace WCFTaskForWorkService
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Auth/Transaction", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Json)]
        Response Transaction(Stream request);

        [OperationContract]
        [WebGet(UriTemplate = "/Public/UserInfo?id={id}", BodyStyle = WebMessageBodyStyle.Bare)]
        Stream UserInfo(int id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Auth/RemoveUser", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Response RemoveUser(SomeWrapper request);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Auth/CreateUser", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
        XElement CreateUser(XElement request);
    }
}
