﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using System.Web;
using System.Web.Management;

using System.Xml.Linq;
using Library;
using System.Globalization;
using System.Net;

namespace WCFTaskForWorkService
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
    public class Service : IService
    {
        static List<UserList> users = null;
        static object Locker = new object();

        public Service()
        {
            users = new List<UserList>();
        }

        public Response Transaction(Stream request)
        {
            Response response = null;
            string[] textRequest = null;

            StreamReader streamReader = new StreamReader(request);
            textRequest = streamReader.ReadToEnd().Split('&');

            int id = int.Parse(textRequest[0]);
            decimal Amount = Convert.ToDecimal(textRequest[1].Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator), CultureInfo.InvariantCulture);
            string type = textRequest[2];

            if (true/*BasicUserAuth()*/)
            {
                UserList userTrans = users.Find(t => t.Id == id);
                if (userTrans != null)
                {
                    if (type == "debit")
                    {
                        if (userTrans.Balance > Amount)
                        {
                            userTrans.Balance = userTrans.Balance - Amount;
                            response = new Response()
                            {
                                user = new UserList() { Id = userTrans.Id, Balance = userTrans.Balance }
                            };
                        }
                        else
                        {
                            response = new Response()
                            {
                                ErrorId = 3,
                                ErrorMsg = "Insufficient funds",
                                Success = false
                            };
                        }
                    }
                    else if (type == "credit")
                    {
                        userTrans.Balance += Amount;
                        response = new Response()
                        {
                            user = new UserList() { Id = userTrans.Id, Balance = userTrans.Balance }
                        };
                    }
                    else
                    {
                        response = new Response()
                        {
                            ErrorId = 4,
                            ErrorMsg = "Invalid request",
                            Success = false
                        };
                    }
                }
                else
                {
                    response = new Response()
                    {
                        ErrorId = 2,
                        ErrorMsg = "User not found",
                        Success = false
                    };
                }
                return response;
            }
            else
            {
                response = new Response()
                {
                    ErrorId = 5,
                    ErrorMsg = "Incorrect login or password",
                    Success = false
                };
                return response;
            }
        }

        public Stream UserInfo(int id)
        {
            string response = string.Empty;
            int a = 5;
            UserList userInfo = users.Find(t => t.Id == id);
            if (userInfo != null)
            {
                response = $"Id = {userInfo.Id}, Name = {userInfo.Name}, Balance = {userInfo.Balance}";

            string result = $"<a href='someLingk' >{response}</a>";
            byte[] resultBytes = Encoding.UTF8.GetBytes(result);
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
            return new MemoryStream(resultBytes);
            }
            else
            {
                response = "ErrorMsg = User not found";
                byte[] resultBytes = Encoding.UTF8.GetBytes(response);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                return new MemoryStream(resultBytes);
            }
        }

        public Response RemoveUser(SomeWrapper request)
        {
            Response response = null;

            try
            {
                int removeId = request.RemoveUser.Id;

                if (true/*BasicUserAuth()*/)
                {
                    UserList removeUser = users.Find(t => t.Id == removeId);
                    if (removeUser != null)
                    {
                        lock (Locker)
                        {
                            response = new Response()
                            {
                                Status = "User was removed",
                                Success = true,
                                user = new UserList() { Id = removeUser.Id, Name = removeUser.Name, Balance = removeUser.Balance }
                            };
                        }
                        users.Remove(removeUser);
                    }
                    else
                    {
                        response = new Response()
                        {
                            ErrorId = 2,
                            ErrorMsg = "User not found",
                            Success = false
                        };
                    }
                    return response;
                }
                else
                {
                    response = new Response()
                    {
                        ErrorId = 5,
                        ErrorMsg = "Incorrect login or password",
                        Success = false
                    };
                    return response;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                response = new Response()
                {
                    ErrorId = 14,
                    ErrorMsg = "Error",
                    Success = false
                };
                return response;
            }

        }

        public XElement CreateUser(XElement request)
        {
            try
            {
                UserList userList;
                XDocument xDoc = new XDocument();
                xDoc.Add(request);

                int id = 0;
                string name = String.Empty;
                decimal balance = 0;

                foreach (XElement Element in xDoc.Element("Request").Elements("user"))
                {
                    XAttribute idAttribute = Element.Attribute("Id");
                    XAttribute nameAttribute = Element.Attribute("Name");
                    XElement balanceElement = Element.Element("Balance");

                    id = int.Parse(idAttribute.Value);
                    name = nameAttribute.Value;
                    balance = Convert.ToDecimal(balanceElement.Value.Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator), CultureInfo.InvariantCulture);
                }

                XAttribute xa;
                if (true/*BasicUserAuth()*/)
                {
                    xa = new XAttribute("Auth", "True");

                    if (balance != 0)
                    {
                        lock (Locker)
                        {
                            userList = new UserList()
                            {
                                Id = id,
                                Name = name,
                                Balance = balance
                            };
                            users.Add(userList);
                        }
                        XElement root = xDoc.Element("Request").Element("user");
                        XElement response = new XElement("Responce", new XAttribute("ErrorId", "0"), new XAttribute("Success", "true"), xa, root);

                        return response;
                    }
                    else
                    {
                        XElement response = new XElement("Responce", new XAttribute("Success", "false"), new XAttribute("ErrorId", "1"), new XElement("ErrorMsg", "User balance must be greater than 0"));
                        return response;
                    }
                }
                else
                {
                    xa = new XAttribute("Auth", "False");
                    XElement response = new XElement("Responce", new XAttribute("Success", "false"), new XAttribute("ErrorId", "5"), new XElement("ErrorMsg", "Incorrect login or password"), xa);
                    return response;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                XElement response = new XElement("Responce", new XAttribute("Success", "false"), new XAttribute("ErrorId", "14"), new XElement("ErrorMsg", "Error"));
                return response;
            }

        }

        bool BasicUserAuth()
        {
            string username = "", password = "";
            IncomingWebRequestContext request = WebOperationContext.Current.IncomingRequest;
            WebHeaderCollection headers = request.Headers;
            string authHeader = headers["Authorization"];

            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                string[] credentials = ExtractCredentials(authHeader);
                username = credentials[0];
                password = credentials[1];
                if (username == "admin" && password == "admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private string[] ExtractCredentials(string authHeader)
        {
            authHeader = authHeader.Replace("Basic ", "");
            byte[] bytes = Convert.FromBase64String(authHeader);
            string logAndPass = Encoding.ASCII.GetString(bytes);
            return logAndPass.Split(':');
        }
    }
}
