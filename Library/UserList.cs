﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel;
using System.Runtime.Serialization;

namespace Library
{
    [DataContract]
    public class UserList
    {
        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember]
        public int Id { get; set; }        

        [DataMember]
        public decimal Balance { get; set; }
    }
}
