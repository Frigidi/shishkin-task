﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel;
using System.Runtime.Serialization;

namespace Library
{
    [DataContract]
    public class RemoveUser
    {
        [DataMember]
        public int Id { get; set; }
    }
}
