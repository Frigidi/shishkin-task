﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel;
using System.Runtime.Serialization;

namespace Library
{
    [DataContract]
    public class Response
    {
        [DataMember(EmitDefaultValue = false)] //EmitDefaultValue = false не будет выводить пустые значения (ноль или null)
        public string Status { set; get; }
        [DataMember(EmitDefaultValue = false)]
        public bool Success { set; get; }
        [DataMember(EmitDefaultValue = false)]
        public UserList user { set; get; }

        [DataMember(EmitDefaultValue = false)]
        public int ErrorId { set; get; }
        [DataMember(EmitDefaultValue = false)]
        public string ErrorMsg { set; get; }
    }
}
