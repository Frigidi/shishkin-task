﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using System.Xml.Serialization;
using Library;
using System.Xml.Linq;
using System.Globalization;
using System.Diagnostics;
using System.Net;

namespace WCFTaskForWorkClient
{
    class Program
    {
        static void RequestResponce(HttpClient client, string url, StringContent content)
        {
            var byteArrayAuth = Encoding.ASCII.GetBytes("admin:admin");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArrayAuth));

            var response = client.PostAsync(url, content).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            //Console.WriteLine();
            Console.WriteLine(responseString);

            //Console.ReadKey();
        }

        static void Main(string[] args)
        {
            HttpClient client = new HttpClient();
            Console.WriteLine("Please any key");
            //Console.ReadKey();
            int id = 999;
            string name = "alex";
            decimal balance = 123.45M;
            ServicePointManager.DefaultConnectionLimit = 5000;
            while (true)
            {
                /*#region Create
                
                    Task.Factory.StartNew(() =>
                    {
                        Console.Write("+");
                        MemoryStream Stream = new MemoryStream();

                        XElement xElem = new XElement("Request", new XElement("user", new XAttribute("Name", name), new XAttribute("Id", id), new XElement("Balance", balance)));

                        XmlSerializerNamespaces xmlNamespace = new XmlSerializerNamespaces();
                        xmlNamespace.Add("", "");
                        XmlSerializer serializer = new XmlSerializer(typeof(XElement));
                        serializer.Serialize(Stream, xElem, xmlNamespace);
                        string xmlObject = Encoding.UTF8.GetString(Stream.ToArray());
                        StringContent content = new StringContent(xmlObject, Encoding.UTF8, "application/xml");
                        string url = "http://localhost:50201/Service.svc/Auth/CreateUser";

                        RequestResponce(client, url, content);
                    });
                #endregion

                #region Delete
                
                    Task.Factory.StartNew(() =>
                    {
                        Console.Write("-");

                        int remId = 999;

                        SomeWrapper sw = new SomeWrapper()
                        {
                            RemoveUser = new RemoveUser() { Id = remId }
                        };
                        string jsonObject = JsonConvert.SerializeObject(sw);

                        var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");
                        var url = "http://localhost:50201/Service.svc/Auth/RemoveUser";

                        RequestResponce(client, url, content);
                    });
                #endregion*/


                Console.Clear();

                Console.WriteLine("Меню: ");
                Console.WriteLine("1: Создает игрока и добавляет информацию о нем в хранилище в памяти.");
                Console.WriteLine("2: Удаляет игрока.");
                Console.WriteLine("3: Возвращает информацию об игроке в виде html страницы.");
                Console.WriteLine("4: Операции с балансом игрока.");
                Console.WriteLine("0: Выход.");
                Console.WriteLine();
                Console.Write("Выберите пункт меню: ");
                int choose = int.Parse(Console.ReadLine());

                bool parseResult;

                switch (choose)
                {
                    case 1:
                        do
                        {
                            Console.Write("Введите id игрока: ");
                            parseResult = int.TryParse(Console.ReadLine(), out id);
                        }
                        while (parseResult == false || id < 0);
                        do
                        {
                            Console.Write("Введите имя игрока: ");
                            name = Console.ReadLine();
                        }
                        while (name == String.Empty);
                        do
                        {
                            Console.Write("Введите баланс игрока: ");
                            balance = Convert.ToDecimal(Console.ReadLine().Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator), CultureInfo.InvariantCulture);
                        }
                        while (balance < 0);
                        MemoryStream Stream = new MemoryStream();

                        XElement xElem = new XElement("Request", new XElement("user", new XAttribute("Name", name), new XAttribute("Id", id), new XElement("Balance", balance)));

                        XmlSerializerNamespaces xmlNamespace = new XmlSerializerNamespaces();
                        xmlNamespace.Add("", "");
                        XmlSerializer serializer = new XmlSerializer(typeof(XElement));
                        serializer.Serialize(Stream, xElem, xmlNamespace);
                        string xmlObject = Encoding.UTF8.GetString(Stream.ToArray());
                        StringContent content = new StringContent(xmlObject, Encoding.UTF8, "application/xml");
                        string url = "http://localhost:50201/Service.svc/Auth/CreateUser";

                        RequestResponce(client, url, content);
                        break;

                    case 2:
                        Console.Write("Введите id игрока для удаления: ");
                        int remId = int.Parse(Console.ReadLine());

                        SomeWrapper sw = new SomeWrapper()
                        {
                            RemoveUser = new RemoveUser() { Id = remId }
                        };
                        string jsonObject = JsonConvert.SerializeObject(sw);

                        content = new StringContent(jsonObject, Encoding.UTF8, "application/json");
                        url = "http://localhost:50201/Service.svc/Auth/RemoveUser";

                        RequestResponce(client, url, content);
                        break;
                    case 3:

                        Console.Write("Введите id игрока, информацию о котором, хотите получить: ");
                        int findId = int.Parse(Console.ReadLine());
                        Uri uri = new Uri("http://localhost:50201/Service.svc/Public/UserInfo?id=" + findId);
                        string result = client.GetStringAsync(uri).Result;

                        Process.Start("http://localhost:50201/Service.svc/Public/UserInfo?id=" + findId);
                        Console.WriteLine(result);
                        Console.ReadKey();

                        //System.IO.File.AppendAllText(@"C:\userInfo.html", "\r\n" + result, Encoding.GetEncoding(1251));
                        //Process.Start(@"C:\userInfo.html");

                        Console.ReadKey();
                        break;
                    case 4:
                        Console.Write("Введите id игрока: ");
                        id = int.Parse(Console.ReadLine());

                        Console.Write("Введите debit - для списания, credit - для пополнения: ");
                        string type = Console.ReadLine();

                        Console.Write("Введите сумму: ");
                        decimal amount = Convert.ToDecimal(Console.ReadLine().Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator), CultureInfo.InvariantCulture);

                        string reqMsg = $"{id}&{amount}&{type}";
                        byte[] byteArray = Encoding.UTF8.GetBytes(reqMsg);
                        MemoryStream mStream = new MemoryStream(byteArray);
                        string strObject = Encoding.UTF8.GetString(mStream.ToArray());

                        content = new StringContent(strObject, Encoding.UTF8, "application/x-www-form-urlencoded");
                        url = "http://localhost:50201/Service.svc/Auth/Transaction";

                        RequestResponce(client, url, content);
                        break;
                    case 0:
                        Environment.Exit(0);
                        break;
                }
            }
        }
    }
}